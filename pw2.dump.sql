-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 29, 2018 at 09:47 AM
-- Server version: 10.0.34-MariaDB-0ubuntu0.16.04.1
-- PHP Version: 5.6.36-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pw2`
--

-- --------------------------------------------------------

--
-- Table structure for table `ordini`
--

CREATE TABLE `ordini` (
  `or_ID` int(11) NOT NULL,
  `or_via` varchar(100) NOT NULL,
  `or_numero_civico` varchar(10) NOT NULL,
  `or_citta` varchar(100) NOT NULL,
  `or_email` varchar(100) NOT NULL,
  `or_telefono` varchar(10) NOT NULL,
  `or_orario` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `or_importo` float NOT NULL,
  `or_stato` tinyint(3) UNSIGNED NOT NULL,
  `or_ora_consegna` text NOT NULL COMMENT 'HH:MM'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ordini`
--

INSERT INTO `ordini` (`or_ID`, `or_via`, `or_numero_civico`, `or_citta`, `or_email`, `or_telefono`, `or_orario`, `or_importo`, `or_stato`, `or_ora_consegna`) VALUES
(23, 'a', 'a', 'a', 'a', 'a', '2018-05-29 08:53:31', 11, 1, '08:53');

-- --------------------------------------------------------

--
-- Table structure for table `prodotti`
--

CREATE TABLE `prodotti` (
  `pr_ID` int(11) NOT NULL,
  `pr_nome` varchar(100) NOT NULL,
  `pr_descrizione` varchar(500) NOT NULL,
  `pr_prezzo` float NOT NULL,
  `pr_eliminato` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `prodotti`
--

INSERT INTO `prodotti` (`pr_ID`, `pr_nome`, `pr_descrizione`, `pr_prezzo`, `pr_eliminato`) VALUES
(1, 'Prodotto 1', 'pr_descrizione', 11, 1),
(2, 'Prodotto 2', 'descrizione', 112, 1),
(3, 'Birra Media', 'Birra a spina 0.5 l', 3.5, 0),
(4, 'Birra Piccola', 'Birra 0.250 l', 2, 0),
(5, 'Birra Grande', 'Birra a spina da 1 l', 5.55, 0),
(6, 'Pizza Margherita', 'La classica pizza Margherita(diametro 30cm)', 3.95, 0),
(7, 'Pizza 4 staggioni', 'La Pizza 4 staggioni(diamettro 30cm)', 5.5, 0),
(8, 'Prodotto 2', 'Descrizione', 11, 1);

-- --------------------------------------------------------

--
-- Table structure for table `riga_ordine`
--

CREATE TABLE `riga_ordine` (
  `ro_ID` int(11) NOT NULL,
  `ro_ID_prodotto` int(11) NOT NULL,
  `ro_ID_ordine` int(11) NOT NULL,
  `ro_quantita` tinyint(3) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `riga_ordine`
--

INSERT INTO `riga_ordine` (`ro_ID`, `ro_ID_prodotto`, `ro_ID_ordine`, `ro_quantita`) VALUES
(29, 7, 23, 1),
(30, 3, 23, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ordini`
--
ALTER TABLE `ordini`
  ADD PRIMARY KEY (`or_ID`);

--
-- Indexes for table `prodotti`
--
ALTER TABLE `prodotti`
  ADD PRIMARY KEY (`pr_ID`);

--
-- Indexes for table `riga_ordine`
--
ALTER TABLE `riga_ordine`
  ADD PRIMARY KEY (`ro_ID`),
  ADD UNIQUE KEY `riga_ordine_UN` (`ro_ID_prodotto`,`ro_ID_ordine`),
  ADD KEY `riga_ordine_ordini_FK` (`ro_ID_ordine`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ordini`
--
ALTER TABLE `ordini`
  MODIFY `or_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `prodotti`
--
ALTER TABLE `prodotti`
  MODIFY `pr_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `riga_ordine`
--
ALTER TABLE `riga_ordine`
  MODIFY `ro_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `riga_ordine`
--
ALTER TABLE `riga_ordine`
  ADD CONSTRAINT `riga_ordine_ordini_FK` FOREIGN KEY (`ro_ID_ordine`) REFERENCES `ordini` (`or_ID`),
  ADD CONSTRAINT `riga_ordine_prodotti_FK` FOREIGN KEY (`ro_ID_prodotto`) REFERENCES `prodotti` (`pr_ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
