-- MySQL dump 10.16  Distrib 10.1.29-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: pw2
-- ------------------------------------------------------
-- Server version	10.1.29-MariaDB-6

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ordini`
--

DROP TABLE IF EXISTS `ordini`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ordini` (
  `or_ID` int(11) NOT NULL AUTO_INCREMENT,
  `or_via` varchar(100) NOT NULL,
  `or_numero_civico` varchar(10) NOT NULL,
  `or_citta` varchar(100) NOT NULL,
  `or_email` varchar(100) NOT NULL,
  `or_telefono` varchar(10) NOT NULL,
  `or_orario` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `or_importo` float NOT NULL,
  `or_stato` tinyint(3) unsigned NOT NULL,
  `or_ora_consegna` text NOT NULL COMMENT 'HH:MM',
  PRIMARY KEY (`or_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ordini`
--

LOCK TABLES `ordini` WRITE;
/*!40000 ALTER TABLE `ordini` DISABLE KEYS */;
INSERT INTO `ordini` VALUES (23,'a','a','a','a','a','2018-05-29 08:53:31',11,1,'08:53'),(24,'Bella Riva','123','Rimini','e@mail.com','1234567891','2018-05-30 09:38:46',22.5,2,'09:40'),(25,'Dante Alighieri','321','Bologna','e@mail.com','9876543219','2018-05-30 09:41:23',35.5,1,'09:00');
/*!40000 ALTER TABLE `ordini` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prodotti`
--

DROP TABLE IF EXISTS `prodotti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prodotti` (
  `pr_ID` int(11) NOT NULL AUTO_INCREMENT,
  `pr_nome` varchar(100) NOT NULL,
  `pr_descrizione` varchar(500) NOT NULL,
  `pr_prezzo` float NOT NULL,
  `pr_eliminato` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pr_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prodotti`
--

LOCK TABLES `prodotti` WRITE;
/*!40000 ALTER TABLE `prodotti` DISABLE KEYS */;
INSERT INTO `prodotti` VALUES (1,'Prodotto 1','pr_descrizione',11,1),(2,'Prodotto 2','descrizione',112,1),(3,'Birra Media','Birra a spina 0.5 l',3.5,0),(4,'Birra Piccola','Birra 0.250 l',2,0),(5,'Birra Grande','Birra a spina da 1 l',5.55,0),(6,'Pizza Margherita','La classica pizza Margherita(diametro 30cm)',3.95,0),(7,'Pizza 4 staggioni','La Pizza 4 staggioni(diamettro 30cm)',5.5,0),(8,'Prodotto 2','Descrizione',11,1);
/*!40000 ALTER TABLE `prodotti` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `riga_ordine`
--

DROP TABLE IF EXISTS `riga_ordine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `riga_ordine` (
  `ro_ID` int(11) NOT NULL AUTO_INCREMENT,
  `ro_ID_prodotto` int(11) NOT NULL,
  `ro_ID_ordine` int(11) NOT NULL,
  `ro_quantita` tinyint(3) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`ro_ID`),
  UNIQUE KEY `riga_ordine_UN` (`ro_ID_prodotto`,`ro_ID_ordine`),
  KEY `riga_ordine_ordini_FK` (`ro_ID_ordine`),
  CONSTRAINT `riga_ordine_ordini_FK` FOREIGN KEY (`ro_ID_ordine`) REFERENCES `ordini` (`or_ID`),
  CONSTRAINT `riga_ordine_prodotti_FK` FOREIGN KEY (`ro_ID_prodotto`) REFERENCES `prodotti` (`pr_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `riga_ordine`
--

LOCK TABLES `riga_ordine` WRITE;
/*!40000 ALTER TABLE `riga_ordine` DISABLE KEYS */;
INSERT INTO `riga_ordine` VALUES (29,7,23,1),(30,3,23,1),(31,7,24,3),(32,4,24,2),(33,7,25,3),(34,4,25,2),(35,5,25,1),(36,6,25,1),(37,3,25,1);
/*!40000 ALTER TABLE `riga_ordine` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'pw2'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-30  9:42:33
