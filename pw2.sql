-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 29, 2018 at 07:23 AM
-- Server version: 10.0.34-MariaDB-0ubuntu0.16.04.1
-- PHP Version: 5.6.36-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pw2`
--

-- --------------------------------------------------------

--
-- Table structure for table `ordini`
--

CREATE TABLE `ordini` (
  `or_ID` int(11) NOT NULL,
  `or_via` varchar(100) NOT NULL,
  `or_numero_civico` varchar(10) NOT NULL,
  `or_citta` varchar(100) NOT NULL,
  `or_email` varchar(100) NOT NULL,
  `or_telefono` varchar(10) NOT NULL,
  `or_orario` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `or_importo` float NOT NULL,
  `or_stato` tinyint(3) UNSIGNED NOT NULL,
  `or_ora_consegna` text NOT NULL COMMENT 'HH:MM'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `prodotti`
--

CREATE TABLE `prodotti` (
  `pr_ID` int(11) NOT NULL,
  `pr_nome` varchar(100) NOT NULL,
  `pr_descrizione` varchar(500) NOT NULL,
  `pr_prezzo` float NOT NULL,
  `pr_eliminato` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `riga_ordine`
--

CREATE TABLE `riga_ordine` (
  `ro_ID` int(11) NOT NULL,
  `ro_ID_prodotto` int(11) NOT NULL,
  `ro_ID_ordine` int(11) NOT NULL,
  `ro_quantita` tinyint(3) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ordini`
--
ALTER TABLE `ordini`
  ADD PRIMARY KEY (`or_ID`);

--
-- Indexes for table `prodotti`
--
ALTER TABLE `prodotti`
  ADD PRIMARY KEY (`pr_ID`);

--
-- Indexes for table `riga_ordine`
--
ALTER TABLE `riga_ordine`
  ADD PRIMARY KEY (`ro_ID`),
  ADD UNIQUE KEY `riga_ordine_UN` (`ro_ID_prodotto`,`ro_ID_ordine`),
  ADD KEY `riga_ordine_ordini_FK` (`ro_ID_ordine`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ordini`
--
ALTER TABLE `ordini`
  MODIFY `or_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `prodotti`
--
ALTER TABLE `prodotti`
  MODIFY `pr_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `riga_ordine`
--
ALTER TABLE `riga_ordine`
  MODIFY `ro_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `riga_ordine`
--
ALTER TABLE `riga_ordine`
  ADD CONSTRAINT `riga_ordine_ordini_FK` FOREIGN KEY (`ro_ID_ordine`) REFERENCES `ordini` (`or_ID`),
  ADD CONSTRAINT `riga_ordine_prodotti_FK` FOREIGN KEY (`ro_ID_prodotto`) REFERENCES `prodotti` (`pr_ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
